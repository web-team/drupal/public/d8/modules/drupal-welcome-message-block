# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 11-02-2021

- Add composer.json
- Update module to be d9-ready (passes readiness scan)

## [1.0.0] - 09-10-2019

- Initial implementation
